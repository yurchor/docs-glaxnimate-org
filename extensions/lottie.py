
import urllib.parse
from pathlib import Path
from typing import List, Tuple

from docutils import nodes
from docutils.parsers.rst import directives

from sphinx.application import Sphinx
from sphinx.environment import BuildEnvironment
from sphinx.transforms.post_transforms import SphinxPostTransform
from sphinx.util.docutils import SphinxTranslator, SphinxDirective
from sphinx.writers.html import HTMLTranslator

from sphinx.util import logging

import shutil
import os

logger = logging.getLogger(__name__)

_ROOT_DIR = os.path.abspath(os.path.dirname(__file__))

SUPPORTED_MIME_TYPES: List[str] = [
    ".rawr",
    ".json",
]

static_files = []

count = 0


class Lottie(SphinxDirective):
    option_spec = {
        'json': directives.unchanged_required,
        'rawr': directives.unchanged_required,
        'height': directives.unchanged_required,
        'width': directives.unchanged_required
    }

    def run(self):
        env: BuildEnvironment = self.env

        rawr = get_file(self.options.get("rawr", ""), env)
        json = get_file(self.options.get("json", ""), env)

        return [
            lottie_node(
                alt=self.options.get("alt", ""),
                autoplay="autoplay" in self.options,
                controls="nocontrols" not in self.options,
                height=self.options.get("height", ""),
                loop="loop" in self.options,
                muted="muted" in self.options,
                poster=self.options.get("poster", ""),
                width=self.options.get("width", ""),
                rawr=rawr,
                json=json,
                klass=self.options.get("class", ""),
            )
        ]


def get_file(src: str, env: BuildEnvironment) -> str:
    """Return file.

    Load the file to the static directory if necessary and process the suffix. Raise a warning if not supported but do not stop the computation.

    Args:
        src: The source of the file (can be local or url)
        env: the build environment

    Returns:
        The src file, whether the file is remote
    """
    suffix = Path(src).suffix
    if suffix not in SUPPORTED_MIME_TYPES:
        logger.warning(
            f'The provided file type ("{suffix}") is not a supported format. defaulting to ""'
        )

    is_remote = bool(urllib.parse.urlparse(src).netloc)
    if not is_remote:
        # Map file paths to unique names (so that they can be put into a single
        # directory). This copies what is done for images by the process_docs method of
        # sphinx.environment.collectors.asset.ImageCollector.
        src, fullpath = env.relfn2path(src, env.docname)
        env.note_dependency(fullpath)
        env.images.add_file(env.docname, src)

    return src


class lottie_node(nodes.General, nodes.Element):
    """Lottie node."""

    pass


class LottiePostTransform(SphinxPostTransform):
    """Ensure files are copied to build directory.

    This copies what is done for images in the post_process_image method of
    sphinx.builders.Builder, except as a Transform since we can't hook into that method
    directly.
    """

    default_priority = 200

    def run(self):
        """Add files to Builder's image tracking.

        Doing so ensures that the builder copies the files to the output directory.
        """
        # TODO: This check can be removed when the minimum supported docutils version
        # is docutils>=0.18.1.
        traverse_or_findall = (
            self.document.findall
            if hasattr(self.document, "findall")
            else self.document.traverse
        )
        for node in traverse_or_findall(lottie_node):
            src = node["json"]
            self.app.builder.images[src] = self.env.images[src][1]
            src = node["rawr"]
            self.app.builder.images[src] = self.env.images[src][1]


def visit_lottie_node_html(translator: HTMLTranslator, node: lottie_node) -> None:
    """Entry point of the html lottie node."""
    # start the lottie block
    global count

    builder = translator.builder

    rawr = node["rawr"]
    json = node["json"]

    if rawr in builder.images:
        rawr = Path(
            builder.imgpath, urllib.parse.quote(builder.images[rawr])
        ).as_posix()

    if json in builder.images:
        json = Path(
            builder.imgpath, urllib.parse.quote(builder.images[json])
        ).as_posix()

    html: str = f'''
    <div class="lottie-container clear-both">
        <div class="alpha_checkered" id="lottie_target_{ count }" style="width:{node["width"]};height:{node["height"]}">
        </div>
        <button class="btn" id="lottie_play_{ count }" onclick="anim[{ count }].play(); document.getElementById('lottie_pause_{ count }').style.display = 'inline-block'; this.style.display = 'none'" style="display:none" title="Play">
            <i class="fa fa-play"></i>
        </button>
        <button class="btn" id="lottie_pause_{ count }" onclick="anim[{ count }].pause(); document.getElementById('lottie_play_{ count }').style.display = 'inline-block'; this.style.display = 'none'" title="Pause">
            <i class="fa fa-pause"></i>
        </button>
        <a href="{ rawr }" title="Download" download="">
            <button class="btn">
                <i class="fa fa-download"></i>
            </button>
        </a>
        <script>
            if (typeof anim === "undefined") {{
                var anim = {{}};
            }}
            anim[{ count }]=lottie.loadAnimation({{
                container: document.getElementById("lottie_target_{ count }"),
                renderer: "svg",
                loop: true,
                autoplay: true,
                path: "{ json }"
            }});
        </script>
'''
    count = count + 1

    translator.body.append(html)


def depart_lottie_node_html(translator: HTMLTranslator, node: lottie_node) -> None:
    """Exit of the html lottie node."""
    translator.body.append("</div>")


def visit_lottie_node_unsuported(translator: SphinxTranslator, node: lottie_node) -> None:
    """Entry point of the ignored lottie node."""
    logger.warning(
        f"lottie {node['json']}: unsupported output format (node skipped)"
    )
    raise nodes.SkipNode


def add_js_files(app, config):
    filename = 'lottie.js'
    checksum = 'sha512-YbdH6CNtUV1DVxoDiiPNA+KyrGc+LItHLbj2w4s14yHvWlyQXc/72jXGIi5rTivTChwlJUudkco8I+1YGjZ70A=='
    app.add_js_file(filename, priority=100, integrity=checksum, crossorigin="anonymous")
    shutil.copyfile(
        os.path.join(_ROOT_DIR, filename),
        os.path.join(app.outdir, '_static', filename)
    )


def add_css_files(app, config):
    filename = 'lottie.css'
    checksum = 'sha-512-kRuhmypiiUS6M4k8pX7Sm9i3owJD4HSoxqLKfonPhXY7aTEIX39BnQmF82ClWl6oHviwrNfixxzbeb8Y9+G+8g=='
    app.add_css_file(filename, priority=100, integrity=checksum, crossorigin="anonymous")
    shutil.copyfile(
        os.path.join(_ROOT_DIR, filename),
        os.path.join(app.outdir, '_static', filename)
    )


def setup(app: Sphinx):

    app.add_node(
        lottie_node,
        html=(visit_lottie_node_html, depart_lottie_node_html),
        epub=(visit_lottie_node_unsuported, None),
        latex=(visit_lottie_node_unsuported, None),
        man=(visit_lottie_node_unsuported, None),
        texinfo=(visit_lottie_node_unsuported, None),
        text=(visit_lottie_node_unsuported, None),
    )
    app.add_directive('lottie', Lottie)
    app.add_post_transform(LottiePostTransform)

    app.connect('config-inited', add_js_files)
    app.connect('config-inited', add_css_files)

    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True
    }

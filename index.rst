.. meta::
   :description: The Glaxnimate User Manual
   :keywords: KDE, Glaxnimate, documentation, user manual, vector animation, 2D, open source, free, help, learn

.. metadata-placeholder

   :authors: - Mattia Basaglia

   :license: Creative Commons License SA 4.0


#################
Glaxnimate Manual
#################

Welcome to Glaxnimate!

.. figure:: /images/main_window/main_window.png
  :align: center
  :width: 500px
  :alt: Main Window

  Glaxnimate's main window

Glaxnimate is a powerful and user-friendly desktop application for vector animation and motion design. It focuses on Lottie and SVG and provides an intuitive interface that makes it easy to create stunning animations.

In this manual, we will guide you through the various components of Glaxnimate's user interface and help you get started with creating your first animation: :ref:`bouncing_ball`.

So let's dive in and discover the magic of Glaxnimate!

###############
Getting started
###############

.. container:: toctile

    .. container:: tile no-descr

        :ref:`introduction`


    .. container:: tile no-descr

         :ref:`Tutorials`


##############
User Interface
##############


.. container:: toctile

    .. container:: tile

       .. figure:: /images/main_window/canvas.png
          :target: user_interface/canvas.html

       :ref:`canvas`
          The canvas is the center of the Glaxnimate interface and is where you will preview and edit your animations.

    .. container:: tile

       .. figure:: /images/main_window/docks.png
          :target: user_interface/docks.html

       :ref:`dockable_views`
          These provide quick access to all of the main functionality of Glaxnimate and can be hidden or re-arranged to fit your taste. Some of the dockable views include the Properties, Layers, and the Timeline.


    .. container:: tile

       .. figure:: /images/main_window/menus_toolbars.png
          :target: user_interface/menus.html

       :ref:`menus`  :ref:`toolbars`
          These work in the same way as most user interfaces and provide access to a variety of tools and options that you'll use to create your animations.


.. toctree::
   :hidden:
     
   introduction
   tutorials
   user_interface
   shapes
   formats


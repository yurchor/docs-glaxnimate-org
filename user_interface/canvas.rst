.. metadata-placeholder

   :authors: - Mattia Basaglia

   :license: Creative Commons License SA 4.0


.. _canvas:

Canvas
======

The canvas is the main drawing area in Glaxnimate.


.. figure:: /images/main_window/canvas.png
  :align: center
  :width: 500px
  :alt: The canvas

  The canvas

Tools
-----

Each tool has its own set of features and interactions, see :ref:`tools` for details.

Moving around
-------------

All movement interactions are done with the middle mouse button / mouse wheel.


Panning
~~~~~~~

Drag while holding the mouse middle button to pan.

Zooming
~~~~~~~

You can scroll with the mouse wheel to zoom in and out, alternatively hold :kbd:`Ctrl` and drag up and down with the middle mouse button.

Rotating
~~~~~~~~

If you hold :kbd:`Shift` while dragging with the middle mouse button, you can rotate the canvas.

Finer control
~~~~~~~~~~~~~

.. figure:: /images/main_window/transform_widget.png
  :align: center
  :width: 400px
  :alt: Transform Widget

  Transform Widget


At the bottom of the window, you have finer controls for the view, they are described as follows:

* |zoom-fit-best| :guilabel:`Fit View` button: resets the view and shows the entire canvas.
* :guilabel:`Zoom` spin box: allows to see and change the zoom level.
* |zoom-in| :guilabel:`Zoom In` button: increases the zoom level.
* |zoom-out| :guilabel:`Zoom In` button: decreases the zoom level.
* |zoom-original| :guilabel:`Reset Zoom` button: resets the zoom to 100%, without affecting panning or rotation.
* :guilabel:`Angle` spin box:   allows to see and change the canvas rotation.
* |rotation-allowed| :guilabel:`Reset Rotation` button: resets the angle to 0, without affecting panning or zoom.




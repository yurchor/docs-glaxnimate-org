.. metadata-placeholder

   :authors: - Mattia Basaglia

   :license: Creative Commons License SA 4.0

.. _tools:

Tools
=====

Select
------

|edit-select| This tool allows you to select items and move them.

Mouse Actions
~~~~~~~~~~~~~

This tool has a multitude of mouse interactions:

Left click: Select the item under the mouse.

Dragging a selected item: Moves the item while you drag it. You can hold :kbd:`Ctrl` to snap the movement to the closest orthogonal axis.

Dragging a handle: Moves the handle, editing the associated property.

Dragging from an area without items: Creates a rectangular selection area, when you release the mouse it will select all items intersecting that area.

Dragging while holding :kbd:`Alt`: Draws a path and when you release the mouse, it will select all items intersecting that path.

Right click: show a context menu with actions regarding the current selection and the item under the mouse.

Double click: edit the item under the mouse

All selection actions (click, draw drag, rectangle drag) will replace the current selection by default, or add to the current selection if you are holding :kbd:`Shift` while releasing the mouse button.

Keyboard Actions
~~~~~~~~~~~~~~~~

If you press backspace or delete you can delete the selected items.

Context Menu
~~~~~~~~~~~~

.. figure:: /images/tools/select_menu.png
   :align: left
   :width: 250px
   :alt: Context Menu

   Context Menu

The context menu with the actions affecting the current selection.

.. rst-class:: clear-both

At the bottom you get a submenu for the item under the mouse cursor:

.. figure:: /images/tools/shape_menu.png
   :align: left
   :width: 250px
   :alt: Shape Menu

   Shape Menu


Edit Nodes
----------

|edit-node| This tool allows you to select and edit Shapes.

Mouse Actions
~~~~~~~~~~~~~

Clicking on bezier nodes will select them (:kbd:`Shift+Click` to add to the selection).

Clicking on shapes, will selected them and enable their editing.

Dragging from an empty spot will create a rectangular selection area to select bezier nodes.

Right clicking on the handles for a bezier node or its tangents will show a context menu

Context Menu
~~~~~~~~~~~~

.. figure:: /images/tools/menu_vertex.png
   :align: left
   :width: 250px
   :alt: Context Menu

   Context Menu

.. figure:: /images/tools/menu_tangent.png
   :align: left
   :width: 250px
   :alt: Context Menu

   Context Menu

.. rst-class:: clear-both

The menu is slightly different depending on whether you clicked on a node or tangent.

The first few actions change the node type.

|show-node-handles| *Remove Tangent*

Removes the selected tangent, making the curve exiting straight as it exits the node in the tangent's direction. If the tangent on the other side has also been removed, the segment between the two nodes will be straight.

|format-remove-node| *Remove Node*

Removes the selected node.

|show-node-handles| *Show Tangents*

Shows the handles for removed tangents.

Draw Bezier
-----------

|draw-bezier-curves| This tool allows you to create [Bezier shapes](../shapes.md#path).

It has the [common shape options](#shape-options).

Mouse Actions
~~~~~~~~~~~~~

Clicking or dragging will enter drawing mode, each subsequent click will add a new point to the shape being created.

Clicking on the starting point will close the curve, create the new shape and exit drawing mode.

Double clicking will add a new point, create the new shape and exit drawing mode.

Dragging will pull on the handles of the last added point.

By default the point will be symmetrical, this means the input and output tangent will have the same size and direction.

If you hold :kbd:`Ctrl` while dragging, the point will become smooth, this means the direction of the tangents will stay the same but they can have different lengths. If you hold Shift instead, it will become a corner: the in tangent will not be modified any more and you can change the out tangent as you please.

Releasing :kbd:`Ctrl` or :kbd:`Shift` will revert the point back to symmetrical.

The different tangent types
***************************

.. figure:: /images/tools/bez_drag_sym.png
   :align: left
   :width: 250px
   :alt: Symmetrical

   Symmetrical

.. figure:: /images/tools/bez_drag_smooth.png
   :align: left
   :width: 250px
   :alt: Smooth

   Smooth

.. figure:: /images/tools/bez_drag_corn.png
   :align: left
   :width: 250px
   :alt: Corner

   Corner

Keyboard Actions
~~~~~~~~~~~~~~~~

Pressing backspace or delete will remove the last point you added.

Pressing enter will create the shape and exit drawing mode.

Pressing escape will remove the unfinished shape and exit drawing mode.

Rectangle
---------

|draw-rectangle| This tool allows you to create [rectangles and squares](../shapes.md#rectangle).

It has the [common shape options](#shape-options).

Mouse Interactions
~~~~~~~~~~~~~~~~~~

You click drag with the left mouse button. The position you pressed the button on will be a corner of the rectangle and the position where you release the button will be the opposite corner.

If you hold :kbd:`Ctrl` while dragging, the rectangle will be a square (all sides with equal length).

If you hold :kbd:`Shift`, the first point will be interpreted as the center of the rectangle rather than a corner.

Clicking (without dragging) on an existing object will switch to the edit tool.

Keyboard Interactions
~~~~~~~~~~~~~~~~~~~~~

Pressing Escape will cancel the rectangle currently being dragged.

Ellipse
-------

|draw-ellipse| This tool allows you to create [ellipses and circles](../shapes.md#ellipse).

It works the same way as the [rectangle tool](#rectangle).

Star / Polygon
--------------

|draw-polygon-star| This tool allows you to create [stars and regular polygons](../shapes.md#polystar).

Mouse Interactions
~~~~~~~~~~~~~~~~~~

You click and drag, the point where you initiated the drag defines the center and the point you release the drag defines one of the corners.

Clicking (without dragging) on an existing object will switch to the edit tool.

Keyboard Interactions
~~~~~~~~~~~~~~~~~~~~~

Pressing Escape will cancel the shape currently being dragged.

Options
~~~~~~~

.. figure:: /images/tools/star_options.png
   :align: left
   :width: 250px
   :alt: Star Options

   Star Options

The combo box at the top allows you to toggle between star and regular polygon mode.

:guilabel:`Spoke Ratio` determines the ratio between the outer and inner radius for stars.

The value goes between 0 and 1, the closer to 0 the pointier the star will look.

:guilabel:`Corners` is the number of corners for the shape (eg: 5 will produce a 5-pointed star or a pentagon).

The other options are the [common shape options](#shape-options).

Shape Options
-------------

.. figure:: /images/tools/shape_options.png
   :align: left
   :width: 250px
   :alt: Shape Options

   Shape Options

These options allow you to select what gets created when you finish drawing the shape.

.. rst-class:: clear-both

* :guilabel:`Group`: Will add a shape group containing the shape. Disabling this will disable the other two.
* :guilabel:`Stroke`: Will create a stroke shape. See the [stroke style view](docks.md#stroke) for information on how to change the stroke style.
* :guilabel:`Fill`: Will create a fill shape. See the [fill style view](docks.md#fill) for information on how to change the stroke style.


Color Picker
------------

|color-picker| Selects a color from the screen.


Mouse Interactions
~~~~~~~~~~~~~~~~~~

Moving the mouse on the canvas will show the color being picked. Clicking assigns the color to the selected shape.

If you click and drag, you will be able to select color outside the canvas.

Options
~~~~~~~

.. figure:: /images/tools/picker_options.png
   :align: left
   :width: 250px
   :alt: Color Picker Options

   Color Picker Options

* :guilabel:`Fill` When clicking, sets the fill color
* :guilabel:`Stroke` When clicking, sets the stroke color

Holding :kbd:`Shift` will toggle the two.
